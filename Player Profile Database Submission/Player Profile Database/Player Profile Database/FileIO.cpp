#include "FileIO.h"
#include <iostream>
#include <fstream>

///<summary>Reads a vector of players from a chosen file
///</summary>
std::vector<Player> readFromFile(std::string fileName)
{
	std::vector<Player> subPlayerList;
	std::fstream inStream;
	//Opens stream
	inStream.open(fileName, std::ios::in | std::ios::out | std::ios::binary);
	if (inStream.good())
	{
		while (!inStream.eof())
		{
			Player inputPlayer;
			//Reads a player name in
			for (unsigned int i = 0; i < PLAYER_NAME_LENGTH && !inStream.eof(); i++)
			{
				inStream.read((char*)&inputPlayer.playerName[i], sizeof(char));
			}
			//Reads a score in
			inStream.read((char*)&inputPlayer.highScore, sizeof(int));
			if (!inStream.eof())
			{
				//Adds the player name and score to the player list
				subPlayerList.push_back(inputPlayer);
			}
		}
		//Closes the stream
		inStream.close();
		return subPlayerList;
	}
	return subPlayerList;
}

///<summary>Takes a vector of players and a fileName and writes the playerList to the fileName
///</summary>
void writeToFile(std::vector<Player> subPlayerList, std::string fileName)
{
	std::fstream outStream;
	//Opens stream
	outStream.open(fileName, std::ios::out | std::ios::binary);
	if (outStream.good())
	{
		//For each player
		for (Player player : subPlayerList)
		{
			//Write playerName to file
			for (char character : player.playerName)
			{
				outStream.write(&character, sizeof(char));
			}
			//Write score to file
			outStream.write((char*)&player.highScore, sizeof(int));
		}
		//Close stream
		outStream.close();
	}
}