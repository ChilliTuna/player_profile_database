#pragma once
#include "Config.h"
#include <string>
#include <vector>

//Player class
class Player
{
public:
	//Contains two member functions
	char playerName[PLAYER_NAME_LENGTH]{};
	int highScore = 0;

};

//Non-Member function that creates a new player from user input (it felt more appropriate
//in this file because it is heavily to do with player. Really just formatting
void NewPlayerFromInput(std::vector<Player>& pList);