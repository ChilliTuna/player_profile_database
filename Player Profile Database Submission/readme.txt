To compile:
1)Open solution in Visual Studio.
2)Go to the "Build" tab at the top of VS.
3)Go to "Configuration manager"
4)Under "Active solution configuration" choose "Release" and close the configuration manager
5)Go back to the "Build" tab
6)Press "Build Solution"

To execute:
1)After compiling, locate the "Player Profile Database" folder
4)Open "Release"
5)Run "Player Profile Database.exe"

How to use:
--To create a new player:
1) Type "New"
2) Type player name
3) Type player score
--To sort by name:
1) Type "Sort name"
--To sort by score:
1) Type "Sort score"
--To search for player:
1) Type "Search"
2) Enter name you want to search for
3) Enter "Yes" or "No" depending on whether you want to change the record you accessed
--To change a record:
1) Search for a player
2) Enter "Yes" when asked if you want to change a record
3) Type the number to the left of the record you wish to change
4) Enter either "Name" or "Score" depending on which you want to change
5) Enter new name/score
--To exit:
1)Type exit