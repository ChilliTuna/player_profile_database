#pragma once
#include "Config.h"
#include "Player.h"
#include <string>
#include <vector>

//Reads from a file and outputs a vector of players
std::vector<Player> readFromFile(std::string fileName);

//Takes a vector of players and a fileName and writes the vector of players to the file
void writeToFile(std::vector<Player> subPlayerList, std::string fileName);
