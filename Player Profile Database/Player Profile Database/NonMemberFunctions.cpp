#include "NonMemberFunctions.h"
#include "Config.h"
#include <iostream>
#include <string>

///<summary>Takes a string and gives it a value from console input
///</summary>
void ModifyStringFromConsole(char* stringToChange, int maxSize)
{
	std::string inputString;
	//Gets user input
	std::getline(std::cin, inputString);
	//If the input is too large, let the user know and ask for input again
	if (inputString.size() > (unsigned int)maxSize)
	{
		std::cout << "That is too long. Enter a number smaller than " << maxSize << std::endl;
		ModifyStringFromConsole(stringToChange, maxSize);
		return;
	}
	//Copy the input to the parameter char*
	strcpy_s(stringToChange, maxSize, inputString.c_str());
}

///<summary>Takes an int and gives it a value from console input
///</summary>
void ModifyIntFromConsole(int& intToChange)
{
	std::string inputString;
	//Gets user input
	std::getline(std::cin, inputString);
	try
	{
		//Tries to convert input to an int
		intToChange = std::stoi(inputString);
	}
	catch (...)
	{
		//If it fails, let the user know and take input again
		std::cout << "That input was not valid. Try again." << std::endl;
		ModifyIntFromConsole(intToChange);
	}
}

///<summary>Takes a vector of players and sorts it by score
///</summary>
void SortVectorByScore(std::vector<Player>& pList, bool (*compareFunction)(int a, int b))
{
	//Variable that keeps track of the number of swaps during each pass
	short numSwap;
	do
	{
		numSwap = 0;
		//Iterates throug each number
		for (unsigned int i = 0; i < pList.size() - 1; i++)
		{
			//Use comparison function
			if (!compareFunction(pList[i].highScore, pList[i + 1].highScore))
			{
				//If it fails, swap the players in the vector
				std::swap(pList[i], pList[i + 1]);
				numSwap++;
			}
		}
	} while (numSwap != 0);
}

///<summary>Takes a vector of players and a comparison algorithm and sorts the vector by playerName
///</summary>
void SortVectorByName(std::vector<Player>& pList, bool (*compareFunction)(int a, int b))
{
	//Variable that keeps track of the number of swaps during each pass
	short numSwap;
	do
	{
		numSwap = 0;
		//Iterates through each word
		for (unsigned int j = 0; j < pList.size() - 1; j++)
		{
			//Iterates through each character in the word
			for (int i = 0; i < sizeof(PLAYER_NAME_LENGTH) - 1; i++)
			{
				//Use comparison function
				if (compareFunction(std::tolower(pList[j].playerName[i]), std::tolower(pList[j + 1].playerName[i])))
				{
					//If it passes, swap the players in the vector
					std::swap(pList[j], pList[j + 1]);
					//Add to number of swaps in pass
					numSwap++;
				}
				//If the character is equal, go to next character
				else if (std::tolower(pList[j].playerName[i]) == std::tolower(pList[j + 1].playerName[i]))
				{
					continue;
				}
				break;
			}
		}
	} while (numSwap != 0);
	//If number of swaps == 0, list is sorted
}

///<summary>Takes a vector of players and allows the user to search for a particular player via input
///</summary>
void SearchInCollection(std::vector<Player>& collection)
{
	//Sorts the input vector
	SortVectorByName(collection);
	std::cout << "Search for...?" << std::endl;
	std::string searchString;
	//Gets the user's input for what the be searched
	std::getline(std::cin, searchString);
	//Defining the initial upper and lower bounds of the search
	int upperBound = collection.size();
	int lowerBound = 0;
	//Performs the actual searching and returns the indexes of all items identical to the user's search
	std::vector<int> results = ScourRange(collection, lowerBound, upperBound, searchString);
	std::cout << "Found " << results.size() << " results." << std::endl;
	//If nothing was found, return.
	if (results.size() == 0)
	{
		return;
	}
	//For each result, print some formatted text to give the user its information
	for (int i : results)
	{
		std::cout << i - results[0] + 1 << ") " << collection[i].playerName << " = " << collection[i].highScore << std::endl;
	}
	std::cout << "Would you like to change a record?" << std::endl;
	while (true)
	{
		char chooseChangeString[20];
		//Gets user's input for whether they wish to change a record
		ModifyStringFromConsole(chooseChangeString, 20);
		//Converts the input to lower case
		ToLower(chooseChangeString);
		//If the user said "yes"
		if (strcmp(chooseChangeString, "yes") == 0)
		{
			std::cout << "Which would you like to change?" << std::endl;
			int modIndex;
			//Ask which one they want to change (based on indexes shown earlier)
			ModifyIntFromConsole(modIndex);
			while (true)
			{
				std::cout << "Name or score?" << std::endl;
				char modString[20];
				//Asks if they want to change the name or the score
				ModifyStringFromConsole(modString, 20);
				ToLower(modString);
				if (modString == "name")
				{
					std::cout << "What do you want to change " << collection[results[modIndex - 1]].playerName << " to?" << std::endl;
					//Changes the name to user input
					ModifyStringFromConsole(collection[results[modIndex - 1]].playerName, PLAYER_NAME_LENGTH);
					return;
				}
				else if (modString == "score")
				{
					std::cout << "What do you want to change " << collection[results[modIndex - 1]].playerName
						<< "'s score of " << collection[results[modIndex - 1]].highScore << " to?" << std::endl;
					//Changes the score to user input
					ModifyIntFromConsole(collection[results[modIndex - 1]].highScore);
					return;
				}
				else
				{
					std::cout << "Oops, that was an invalid input. Try again." << std::endl;
				}
			}
		}
		//If the user said "no", close the search function
		else if (strcmp(chooseChangeString, "no") == 0)
		{
			return;
		}
		else
		{
			std::cout << "That was not a valid input. Try again." << std::endl;
		}
	}
}

///<summary>Performs the functionality of searching the collection
///</summary>
std::vector<int> ScourRange(std::vector<Player>& collection, int lowerBound, int upperBound, std::string searchString, short charIndex)
{
	std::vector<int> positions;
	int middleVal;
	//Provided that the sum of the upper and lower bound is > than 1, find the average of the two. Otherwise assume its 0
	if (lowerBound + upperBound > 1)
	{
		middleVal = (lowerBound + upperBound) / 2;
	}
	else
	{
		middleVal = 0;
	}
	//If the final value isn't the desired word or the number or the index of char in a string is higher than the length
	//of the string to be found, return a blank vector
	if ((lowerBound == upperBound && collection[middleVal].playerName != searchString) || charIndex > sizeof(searchString))
	{
		return positions;
	}
	//Otherwise if the desired string is found, search near it to see how many identical players there are then return their indexes
	else if (lowerBound == upperBound && collection[middleVal].playerName == searchString)
	{
		FindRangeFromCenter(collection, lowerBound, upperBound, searchString, middleVal, charIndex);
		for (int i = 0; i <= upperBound - lowerBound; i++)
		{
			positions.push_back(i + lowerBound);
		}
		return positions;
	}
	//If the current observed char is greater than the desired char, call this function again
	if (collection[middleVal].playerName[charIndex] > searchString[charIndex])
	{
		positions = ScourRange(collection, lowerBound, middleVal, searchString);
	}
	else if (collection[middleVal].playerName[charIndex] < searchString[charIndex])
	{
		//If the current observed char is less than the desired char, call this function again
		positions = ScourRange(collection, middleVal, upperBound, searchString);
	}
	else
	{
		//If the current observed string is the desired string, search near it to see how many
		//identical players there are then return their indexes
		if (collection[middleVal].playerName == searchString)
		{
			FindRangeFromCenterFull(collection, lowerBound, upperBound, searchString, middleVal);
			for (int i = 0; i <= upperBound - lowerBound; i++)
			{
				positions.push_back(i + lowerBound);
			}
			return positions;
		}
		//Search near the current observed string to see how many identical chars there are then use
		//that to readjust the search area
		FindRangeFromCenter(collection, lowerBound, upperBound, searchString, middleVal, charIndex);
		//Call this function again with the new range
		positions = ScourRange(collection, lowerBound, upperBound, searchString, charIndex + 1);
	}
	return positions;
}

///<summary>Finds how far to the positive and negative that the char is the same as the initial one
///</summary>
void FindRangeFromCenter(std::vector<Player> collection, int& minRange, int& maxRange,
	std::string searchString, int initialIndex, int charIndex)
{
	//These variables show the offset from the initial index to the furthest identical item's index
	int negIndex = 0, posIndex = 0;
	while (true)
	{
		//If this is 0, the range has been found. This increases by one for each change in negIndex or posIndex
		int increaseNum = 0;
		//Checks that the next element to be compared is within the array
		if (initialIndex - negIndex >= 0)
		{
			//Compares the element to chosen string, to see if it is the same
			if (collection[initialIndex - negIndex].playerName[charIndex] == searchString[charIndex])
			{
				//If so, move to next element
				negIndex++;
				increaseNum++;
			}
			else
			{
				//Otherwise, the furthest elemeent has been found
				minRange = initialIndex - (negIndex - 1);
			}
		}
		else
		{
			//If not, the furthest elemeent has been found
			minRange = initialIndex - (negIndex - 1);
		}
		//Same as above, but for the positive range
		if (initialIndex + posIndex < (int)collection.size())
		{
			if (collection[initialIndex + posIndex].playerName[charIndex] == searchString[charIndex])
			{
				posIndex++;
				increaseNum++;
			}
			else
			{
				maxRange = initialIndex + (posIndex - 1);
			}
		}
		else
		{
			maxRange = initialIndex + (posIndex - 1);
		}
		if (increaseNum == 0)
		{
			return;
		}
	}
}

///<summary>Finds how far to the positive and negative that the string is the same as the initial one
///</summary>
void FindRangeFromCenterFull(std::vector<Player> collection, int& minRange, int& maxRange,
	std::string searchString, int initialIndex)
{
	//These variables show the offset from the initial index to the furthest identical item's index
	int negIndex = 0, posIndex = 0;
	while (true)
	{
		//If this is 0, the range has been found. This increases by one for each change in negIndex or posIndex
		int increaseNum = 0;
		//Checks that the next element to be compared is within the array
		if (initialIndex - negIndex >= 0)
		{
			//Compares the element to chosen string, to see if it is the same
			if (collection[initialIndex - negIndex].playerName == searchString)
			{
				negIndex++;
				increaseNum++;
				//If so, move to next element
			}
			else
			{
				//Otherwise, the furthest elemeent has been found
				minRange = initialIndex - (negIndex - 1);
			}
		}
		else
		{
			//If not, the furthest elemeent has been found
			minRange = initialIndex - (negIndex - 1);
		}
		//Same as above, but for the positive range
		if (initialIndex + posIndex < (int)collection.size())
		{
			if (collection[initialIndex + posIndex].playerName == searchString)
			{
				posIndex++;
				increaseNum++;
			}
			else
			{
				maxRange = initialIndex + (posIndex - 1);
			}
		}
		else
		{
			maxRange = initialIndex + (posIndex - 1);
		}
		if (increaseNum == 0)
		{
			return;
		}
	}
}

///<summary>Converts a char* to lower case
///</summary>
void ToLower(char* inputString)
{
	//Iterates through each char and converts it to lowercase
	for (int i = 0; i < sizeof(inputString); i++)
	{
		inputString[i] = tolower(inputString[i]);
	}
}

///<summary>Converts a char* to a string
///</summary>
std::string ToString(char* charArray)
{
	std::string returnString;
	//Iterates through all chars in the char* and puts them into a string
	for (int i = 0; i < sizeof(charArray); i++)
	{
		returnString[i] = charArray[i];
	}
	return returnString;
}