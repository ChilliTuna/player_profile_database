#pragma once
#include <vector>
#include "Player.h"

//Takes a string and gives it a value from console input
void ModifyStringFromConsole(char* stringToChange, int maxSize);

//Takes an int and gives it a value from console input
void ModifyIntFromConsole(int& intToChange);

//Takes a vector of players and sorts it by score
void SortVectorByScore(std::vector<Player>& pList, bool (*compareFunction)(int a, int b) = [](int a, int b) {return a <= b; });

//Takes a vector of players and sorts it by name
void SortVectorByName(std::vector<Player>& pList, bool (*compareFunction)(int a, int b) = [](int a, int b) {return a > b; });

//Takes a vector of players and allows the user to search for a particular player via input
void SearchInCollection(std::vector<Player>& collection);

//Performs the functionality of searching the collection
std::vector<int> ScourRange(std::vector<Player>& collection, int upperbound, int lowerbound, std::string searchString, short charIndex = 0);

//More search stuff
void FindRangeFromCenter(std::vector<Player> collection, int& minRange, int& maxRange, std::string searchString, int initialIndex, int charIndex);

//" "
void FindRangeFromCenterFull(std::vector<Player> collection, int& minRange, int& maxRange, std::string searchString, int initialIndex);

//Converts a char* to lower case
void ToLower(char* inputString);

//Converts a char* to a string
std::string ToString(char* charArray);