#include <iostream>
#include "NonMemberFunctions.h"
#include "FileIO.h"
#include "Player.h"

int main()
{
	//Reads all player data from file and writes it to a list
	std::vector<Player> playerList = readFromFile(PLAYER_DATA_FILE);
	while (true)
	{
		//Prints the stats of all players
		for (Player player : playerList)
		{
			std::cout << player.playerName << " has a score of: " << player.highScore << std::endl;
		}
		std::cout << "Enter \"New\", \"Sort name\", \"Sort score\", \"Search\" or \"Exit\"." << std::endl;

		//Takes user input and compares it to the possible options
		char doWhat[20];
		//Takes input
		ModifyStringFromConsole(doWhat, 20);
		//Converts input to lower case
		ToLower(doWhat);
		//Clears console (for readability)
		system("CLS");
		//If input is "new", create new player
		if (strcmp(doWhat, "new") == 0)
		{
			NewPlayerFromInput(playerList);
		}
		//If input is "sort name", sort by name
		else if (strcmp(doWhat, "sort name") == 0)
		{
			SortVectorByName(playerList);
		}
		//If input is "sort score", sort by score
		else if (strcmp(doWhat, "sort score") == 0)
		{
			SortVectorByScore(playerList);
		}
		//If input is "search", search for a player
		else if (strcmp(doWhat, "search") == 0)
		{
			SearchInCollection(playerList);
		}
		//If input is "exit", exit program
		else if (strcmp(doWhat, "exit") == 0)
		{
			break;
		}
		else
		{
			std::cout << "Invalid input." << std::endl;
		}
		std::cout << std::endl;
	}
	//Write the list of players back to the file
	writeToFile(playerList, PLAYER_DATA_FILE);
	return 0;
}