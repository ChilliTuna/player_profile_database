#include "Player.h"
#include "NonMemberFunctions.h"
#include <iostream>
#include <string>

//Non-Member function that creates a new player from user input (it felt more appropriate
//in this file because it is heavily to do with player. Really just formatting
///<summary>Takes a player list and adds a new player, based on user input
///</summary>
void NewPlayerFromInput(std::vector<Player>& pList)
{
	Player newPlayer;
	std::cout << "Enter name: " << std::endl;
	ModifyStringFromConsole(newPlayer.playerName, PLAYER_NAME_LENGTH);
	std::cout << "Enter highscore: " << std::endl;
	ModifyIntFromConsole(newPlayer.highScore);
	pList.push_back(newPlayer);
}
